
This file should describe your design choices and motivates the choices.

The code reads command line arguments when the Java program is run. 
It spawns any process that exists in the system path without having to give directory.
This done by the spawn method found in the ProcessManager class. 

One the process is done it can be destroyed to end process. This is done through a separate destroy method, which returns a boolean if it was successfully destroyed the process.

At any point in the life cycle of the program, it can be checked if the process still exists or not. To do so, there is a separate method for it named is-alive.

For testing purposes, "javac -version" is tested.

spawnAndCollect() method allows us to spawn a process and read any text the process has given out.
To read the input text stream, inputStreamReader method provided by the process was used. It returns 
ASCII decimals values which needs to by type-casted to a character before a string. To test this method is functional, first, "javac -version" was tested to return the current java compiler version in my computer was read correctly. then "notepad" was opened. All test must pass upon closing notepad.