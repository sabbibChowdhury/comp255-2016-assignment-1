package exception;

 
public class TimeOutException extends Exception {
	 
	public TimeOutException(){
		super("Process is taking too long");
	}   

	public String toString(){
		return "Process is taking too long";
	}
}