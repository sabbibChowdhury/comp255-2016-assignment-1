import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.InterruptedByTimeoutException;
import java.util.regex.Pattern;

import exception.TimeOutException;

/**
 * Create a running process and manage interaction with it
 */
public class ProcessManager  {

	/**
	 * The strings for which the analyser is built
	 */
	Process process ;
	String      program;
	String[]    arguments;
	protected String exception =  null;
	protected BufferedReader br ;

	/**
	 * Make aa running process with
	 *
	 *  @param  executable    The program to run
	 *  @param  args          The arguments of the program
	 */
	public ProcessManager(String executable, String[] args) {
		program = executable;
		arguments = args;
	}

	/**
	 * Expects a regex pattern p from the process
	 * @param p
	 * @param timeoutMilli
	 * @return String expected
	 * @throws InterruptedByTimeoutException 
	 */
	public String expect(Pattern p, int timeoutMilli) throws TimeOutException{//TODO
		String input = collectNextLine();
		String expected[];
		long startTimer = System.currentTimeMillis();
		while(System.currentTimeMillis() - startTimer < timeoutMilli){
			try{
				expected = p.split(input);
				if(p.matcher(input).find())
					for(int i = 0; i <= expected.length; i++)
						if(!expected[i].equals("\n") || !expected[i].equals("\r"))
							return expected[0];
			}catch(Exception e){
				e.printStackTrace();
				//			return new InterruptedException();
			}
		}
		return new TimeOutException().toString(); 
//		throw new TimeOutException(); 
		//for the purpose of this assignment, the return value is a string
	}//end expect()


	/**
	 * Sends string s to the process
	 * @param s
	 * @return
	 */
	public boolean send(String s){
		try{
			if(process.isAlive()){
				OutputStream test  = process.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(test));
				try {
					writer.write(s);
					writer.newLine();
					writer.flush();
					return true;
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}catch(NullPointerException e){
			return false;
		}
		return false;
	}

	/**
	 * Spawn a process
	 */
	public boolean spawn() throws Exception {
		if(program != null && program.length() >0){
			process =  new ProcessBuilder(new String[] {program}).start();
			br = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			return true;
		}else{
			System.out.println("Did not run");
			return false;
		}
	}//end spawn()

	/**
	 * Spawn a process and collect the results
	 */
	public String spawnAndCollect() {
		ProcessBuilder pb  = new ProcessBuilder(arguments);
		try{
			pb.redirectErrorStream(true);
			process = pb.start();
			String sLine = "";
			int c ;
			while((c = process.getInputStream().read()) != -1){
				sLine += (char)c;
			}
			return sLine;
		}catch(Exception e){
			e.printStackTrace();
		}

		return "not found" ;
	}

	/**
	 * Spawn a process and collect the results or throw an
	 * exception if no answer before the timeout
	 *
	 * @param  timeout     The timeout in milliseconds
	 * @throws Exception 
	 */
	public String spawnAndCollectWithTimeout(int timeOutSecond) throws Exception {
		String result = "";
		long current = System.currentTimeMillis();

		ProcessBuilder pb  = new ProcessBuilder(arguments);
		try{
			pb.redirectErrorStream(true);
			process = pb.start();
			int c ;
			while((c = process.getInputStream().read()) != -1 )
				if(System.currentTimeMillis() - current < timeOutSecond) 
					result += (char)c;
				else throw new Exception();
			return result.trim();
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public String collectNextLine(){
		try {
			if(process.isAlive()) {
				if(br.ready()) return br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			return new TimeOutException().toString();
		}
		return "";
	}

	/**
	 * Kill the process
	 */
	public boolean destroy() {
		if(isAlive()){
			process.destroy();
			return true;
		}else{
			return false;
		}
	}//end destroy()


	public String processExists(){
		if(process != null) 	return "Exists";
		else			return "Doesn't exists";
	}

	public boolean isAlive(){
		if(process != null) 	return process.isAlive();
		else 			return false;
	}

	public String getErrorMessage(){
		return exception;
	}

	public int exit(){		return process.exitValue();	}

	
}//end ProcessManager.class