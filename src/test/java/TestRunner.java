
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {

	 static String[] ARGS;
	 static TestRunner tr;
	 
	
    public static void main(String args[]) {
    	tr = new TestRunner();
    	
        TestRunner.ARGS = tr.setARGS(args);
        
        Result res = JUnitCore.runClasses(ProcessManagerTests.class);

        if (res.wasSuccessful() != true) {
            System.out.println("Some tests failed: ");
            //  print the failures
            for (Failure f: res.getFailures()) {
                System.out.println("[Test failed] " + f.toString());
            }
        } else { 
            System.out.println("All tests passed!");
        }
       
    }//end main
    
    
    public String[] setARGS(String[] a){
    	return ARGS = a;
    }//end setCommandLineArgs()
    
    public String[] getARGS(){
    	return ARGS;
    }//end getCommandLineArgs
    
}//end TestRunner.class
