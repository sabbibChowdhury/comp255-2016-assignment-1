
import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.rules.Timeout;

import exception.TimeOutException;


/**
 * Test suite for Longest Common Subsequences
 */
public class ProcessManagerTests {

	String PROG;
	String[] ARGS;
	TestRunner TR;
	boolean DESTROY ;
	
	
    /**
     * Test for spawning a process
     */
    @Test
    public void templateTest() {
    	TR = new TestRunner();
        //  FIXME
        //  program to spawn and arguments
        String prog = getProg();
        String[] args = getAllArgs();

        //  create a process manager to interact with `ps`
        ProcessManager pm = new ProcessManager(prog, args);
        //  spawn a process that runs `ps aux` and collect the result
       
        try {
        	/***
        	 * Returns false when the program is not running
        	 */
        	DESTROY = pm.destroy();
        	assertEquals(false, DESTROY);
        	System.out.println("Is process destoryed? " + pm.destroy());
        	
        	
        	/***
        	 * Returns true when the program is running
        	 * 
        	 * NOTE: pm takes variables prog and args, from 
        	 * TestRunner class's input string arguments from main method.
        	 * Spawning pm from JUnit will not run correctly as instantiating
        	 * class TestRunner was not been run.
        	 */
			//NOTE: RUN FROM TestRunner.java only
			assertEquals(true, pm.spawn());
			System.out.println("Is process spawn? " + pm.spawn());
			
			/***
        	 * Destroys the process
        	 */
			DESTROY = pm.destroy();
			assertEquals(true, DESTROY);
			System.out.println("Is process destoryed? " + DESTROY);
			
			/**
	         * Run a program and collect any out from it.
	         * Run javac -version
	         */
			pm = new ProcessManager(prog, args);
	        String res = pm.spawnAndCollect();
	        System.out.println(res.trim() + " is opened");
	        assertEquals("javac 1.8.0_73", res.trim());
			pm.send("String");
	        
	        /**
	         * Spawns process,
	         * Collects output from process when argument is correct,
	         * checks if output is correctly collected by process.
	         */
	        
	        prog = "notepad"; 
	        args[0] = "notepad";args[1] = "";
	        pm = new ProcessManager(prog, args);
	        res = pm.spawnAndCollect();	        
	        assertEquals("", res);
	        
	        
	        /**
	         * Spawns process,
	         * Collects output from process when argument is correct,
	         * checks if output is correctly collected by process.
			 * time in seconds	 
	         */
	        System.out.println("timeout 1");
	        pm.destroy();
	        args[0] = "javac"; args[1] = "-version";
	        String s0 = "javac 1.8.0_73";
	        assertEquals(s0, pm.spawnAndCollectWithTimeout(1));
	       
	        
	        /**
	         * Spawns process,
	         * Collects output from process when argument is incorrect,
	         * checks if output is correctly collected by process.
	         * Correct output with time limit; time in seconds.	        
	         */
	        pm.destroy();
	        String s1 = "error: Class names, 'version', "
	        		+ "are only accepted if annotation "
	        		+ "processing is explicitly requested\r\n1 error";

	        args[0] = "javac"; args[1] = "version";
	        assertEquals(s1,
	        		pm.spawnAndCollectWithTimeout(1));
	        
			/**
			 * Spawns process,
	         * Collects output from process when argument is correct,
	         * checks if output is correctly collected by process.
	         * Collects output with time limit; time in milliseconds	        
			 */
	        pm.destroy();
	        String s2 = "javac 1.8.0_73";
	        
	        args[0] = "javac"; args[1] = "-version";
	        assertEquals(s2,
	        		pm.spawnAndCollectWithTimeout(1000));
	        
	        
	        /**
			 * Spawns process,
	         * Collects output from process when argument is incorrect,
	         * checks if output is correctly collected by process.
	         * Collects output with time limit; time in milliseconds	        
			 */
	        pm.destroy();
	        String s3 = "error: Class names, 'version', "
	        		+ "are only accepted if annotation "
	        		+ "processing is explicitly requested\r\n1 error";
	        
	        args[0] = "javac"; args[1] = "version";
	        assertEquals(s3,
	        		pm.spawnAndCollectWithTimeout(1000));
	        
	        
	        /**
	         * Spawns process,
	         * Collects output from process when argument is correct,
	         * under time limit.
	         * Expected to fail and return correct stack trace message
	         */
	        pm.destroy();
	        args[0] = "javac"; args[1] = "-version";
	        assertEquals(
	        		new TimeOutException().getMessage()
	        		, pm.spawnAndCollectWithTimeout(20));
	       
	        
	        
	        /**
	         * Spawns process,
	         * Collects output from process when argument is incorrect,
	         * under time limit.
	         * Expected to fail and return correct stack trace message
	         * Incorrect input should be unaffected;
	         */	        
	        args[0] = "javac"; args[1] = "version";
	        assertEquals(
	        		new TimeOutException().getMessage()
	        		, pm.spawnAndCollectWithTimeout(20));

	        
	        pm.exit();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
        
    }
    
    @Test
    public void expectMultipleTestStandard(){
    	ProcessManager pm = new ProcessManager("expectMultiple.bat", new String[] {""});
    	try {
    		assertEquals(new TimeOutException().toString(), pm.expect(Pattern.compile(":"), 3_000));
    		pm.spawn();
/*ignore*/  System.out.println(pm.collectNextLine());//ignore
    		
			/**
			 * prompt return password 
			 */
			String expectString = pm.expect(Pattern.compile(":"), 1_000);
    		assertEquals("Enter password", expectString);
    		System.out.println(expectString);
    		
    		//sending password
    		pm.send("password123");
    		
    		/**
    		 * prompt return connected
    		 */
    		expectString = pm.expect(Pattern.compile("(\\.+)"), 1_000);
    		assertEquals("connected", expectString);
    		System.out.println(expectString);
    		
    		/**
    		 * prompt return bash:>>
    		 */
    		expectString = pm.expect(Pattern.compile("\\>"), 1_000);
    		assertEquals("bash:", expectString);
    		System.out.println(expectString);
    		
    		pm.destroy();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }//end expectMultipleTest()
    
    
    @Test
    public void expectTest(){
    	ProcessManager pm = new ProcessManager("expect.bat", new String[] {""});
    	try {
    		assertEquals(new TimeOutException().toString(), pm.expect(Pattern.compile(":"), 1_000));
			assertEquals(pm.spawn(), true);
			
			String res = pm.expect(Pattern.compile(":"), 1000);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }//end expectTest()
    //1300 304 647
    @Test
    public void sendTest(){
    	ProcessManager pm = new ProcessManager("255.bat", new String[] {""});
    	try {
    		assertEquals(false, pm.send("test"));
			pm.spawn();
			pm.collectNextLine();//ignore this
			boolean sendResult  = pm.send("test");
			assertEquals("test", pm.collectNextLine());
			assertEquals(true, sendResult);
		} catch (Exception e) { e.printStackTrace(); 		}
    		
    }//end sendTest();
  
    
    private String getProg(){
    	if(TR != null && TR.getARGS() != null)     	
    		return PROG = TR.getARGS()[0];
    	else return "";
    }
    
    private String[] getAllArgs(){
    	if(TR.getARGS() == null){
    		ARGS = new String[1]; 
    		ARGS[0] = "";
    		return ARGS; 
    	}   	
    	return ARGS = TR.getARGS();
    }
}